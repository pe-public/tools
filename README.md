[[_TOC_]]

Helper scripts for Users module
-------------------------------

Ruby scripts to add and remove users from "users" module hiera datasource. POSIX attributes
of a user account are matching ones in LDAP. Ruby has been chosen because it does not require
installation of any additional stuff (modules, packages, etc.) on a Mac PAW.

## Prerequisites

* The scripts assume that they live in `tools` directory in the root of the repo.
* A user running this script _must_ have a kerberos ticket.
* Kerberos principal must be allowed to run remctl command `remctl lsdb user show`

## Usage

Add a user

```
./add_user sunet_id
```

Delete user:

```
./del_user sunet_id
```

List users:

```
./list_users sunet_id
```

Refresh users:

```
./refresh_users sunet_id
```

The above are all wrapper scripts around `manage-user` and take the same
options as `manage-user`. For more options, see the section "Advanced
options" below.

## Notes

* The script also determines if a user has a base or fully sponsored SUNet account.
If a user has a base account, i.e. no AFS space, the script forces his home directory
in hiera data source to be set to /home/sunet_id. Users module relies on this
information to automatically put base account homes locally and create .k5login
files for them.

## Configuration

You can set script configuration using command-line options or by using
the file `manage-user.yml`. The file `manage-user.yml` _must_ be in the
same directory as the `manage-user` script.  The configuration options you
can set are `classname`, `git-commit`, and `verbose`. Example:
```
---
# ./manage-user.yml
# Recognized configuration options:
#
# - classname
# - git-commit
# - verbose

# Where to find the user data:
classname:  defaults::iedo_users

# Set git-commit to true to do a git auto-commit, false otherwise:
git-commit: false

# Set verbose to true to get more verbose output:
verbose: true
```

Note: If you have an option in the `manage-user.yml` configuration file
_and_ you set the same configuration option via a command-line option, the
command-line option setting will be the one used.

## Advanced options

By default, the scripts assume that the YAML file where the
user information is stored is in `modules/users/data/common.yaml` in this format:
```
users::stanford_users:
  adamhl:
    uid: 12345
    gid: 37
    comment: Adam Henry Lewenberg
    home: "/afs/ir/users/a/d/adamhl"
    shell: "/bin/tcsh"
  ...
```

If you keep this file in a _different_ location, use the `classname` configuration setting in
`manage-user.yml` (or the `-c` command-line option). For example:
```
$ ./add-user adamhl -c defaults::iedo_users
```
will look for the file `modules/defaults/data/common.yaml` which should have the format
```
defaults::iedo_users:
  adamhl:
    uid: 12345
    gid: 37
    comment: Adam Henry Lewenberg
    home: "/afs/ir/users/a/d/adamhl"
    shell: "/bin/tcsh"
  ...
```
